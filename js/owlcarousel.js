(function ($, Drupal) {
    'use strict';
    Drupal.behaviors.owlcarousel = {
        attach: function (context, settings) {
            $('.owl-slider-wrapper', context)
            .once("owlcarousel")
            .each(
                function () {
                    var $this = $(this);
                    var $thisSettings = $.parseJSON($this.attr('data-owlcarousel-settings'));
                    console.log($thisSettings);
                    $this.owlCarousel($thisSettings);
                }
            );
        },
        detach: function (context, settings, trigger) {
        }
    };
})(jQuery, Drupal);