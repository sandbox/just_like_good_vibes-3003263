OwlCarousel 8.x
-----------------

### About this module
This is an experimental module to integrate [OwlCarousel 2 javascript library](http://owlcarousel2.github.io/OwlCarousel2/) with Drupal 8 Views.

The primary use case of this module is to create a view and set up a display of type OwlCarousel.
The views display options will allow you to pass directives for the OwlCarousel library.

## Installation
Install as you would normally install a contributed Drupal module. See the
[Drupal 8 Instructions](https://drupal.org/documentation/install/modules-themes/modules-8)
if required in the Drupal documentation for further information. 

The module requires you to download the OwlCarousel 2 javascript library in the library folder of Drupal.
You have two possibilities:
  - you manually download the code and place it under library/owlcarousel folder. Please use the 2.x branch. You can pick for example the following archive : https://github.com/OwlCarousel2/OwlCarousel2/archive/2.3.4.zip or select one version available from the github page https://github.com/OwlCarousel2/OwlCarousel2/releases.
  - you use composer to manage download and version selection. OwlCarousel 2 javascript library is available on the composer repository https://asset-packagist.org under the name "npm-asset/owl.carousel".

After download of the library, the file owl.carousel.min.css should be available at the following location relative to the root directory of Drupal : library/owl.carousel/dist/assets/owl.carousel.min.css
