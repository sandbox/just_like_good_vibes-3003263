<?php
/**
 * Definition of OwlCarouselOptionsInterface
 */

namespace Drupal\owlcarousel;

use Drupal\Core\Form\FormStateInterface;

/**
 * Interface OwlCarouselOptionsInterface.
 */
interface OwlCarouselOptionsInterface
{
    public function defaultSettings();
    public function formatSettings($settings);
    public function settingsForm(&$form, FormStateInterface $form_state, &$options);
    public function validateForm(&$form, FormStateInterface $form_state);
}
