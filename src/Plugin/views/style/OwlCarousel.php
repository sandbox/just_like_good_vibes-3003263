<?php
// https://github.com/tabvn/owl/blob/master/src/Plugin/views/style/Owl.php
namespace Drupal\owlcarousel\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\owlcarousel\OwlCarouselOptionsInterface;

/**
 * Style plugin to render each item in an owl carousel.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "owlcarousel",
 *   title = @Translation("OwlCarousel2"),
 *   help = @Translation("Displays rows as Owl Carousel"),
 *   theme = "views_view_owlcarousel",
 *   display_types = {"normal"}
 * )
 */

class OwlCarousel extends StylePluginBase
{

    /**
     * {@inheritdoc}
     */
    protected $usesRowPlugin = true;

    /**
     * Does the style plugin support custom css class for the rows.
     *
     * @var bool
     */
    protected $usesRowClass = true;


    /**
     * Does the style plugin support custom css class for the rows.
     *
     * @var \Drupal\owlcarousel\OwlCarouselOptionsInterface
     */
    protected $owlcarousel_options;

    /**
     * Constructs a Drupal\owlcarousel\Plugin\views\style\OwlCarousel object.
     *
     * @param array                                           $configuration
     *   A configuration array containing information about the plugin instance.
     * @param string                                          $plugin_id
     *   The plugin_id for the plugin instance.
     * @param mixed                                           $plugin_definition
     *   The plugin implementation definition.
     * @param \Drupal\owlcarousel\OwlCarouselOptionsInterface $owlcarousel_options
     *   The owlcarousel options manager
     */
    public function __construct(array $configuration, $plugin_id, $plugin_definition, OwlCarouselOptionsInterface $owlcarousel_options)
    {
        parent::__construct($configuration, $plugin_id, $plugin_definition);
        $this->owlcarousel_options = $owlcarousel_options;
    }


    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('owlcarousel.options')
        );
    }

    /**
     * Set default options
     */
    protected function defineOptions()
    {
        $options = parent::defineOptions();
        $settings = $this->owlcarousel_options->defaultSettings();
        $default_options =  array_merge($options, $settings);
        //\Drupal::logger("owl_carousel")->info("def<pre>".print_r($default_options, TRUE)."</pre>");
        return $default_options;

    }

    /**
     * Render the given style.
     */
    public function buildOptionsForm(&$form, FormStateInterface $form_state)
    {
        parent::buildOptionsForm($form, $form_state);
        $this->owlcarousel_options->settingsForm($form, $form_state, $this->options);
    }

    public function validateOptionsForm(&$form, FormStateInterface $form_state)
    {
        \Drupal::logger("owlcarousel")->info("validate");
        parent::validateOptionsForm($form, $form_state);
        $this->owlcarousel_options->validateForm($form, $form_state);
    }
    public function submitOptionsForm(&$form, FormStateInterface $form_state)
    {
        parent::submitOptionsForm($form, $form_state);
        $values = $form_state->getValues();
        \Drupal::logger("owlcarousel")->info("submit <pre>".print_r($form_state->getValue("owlcarousel_raw"), true)."</pre>");
    }
}

