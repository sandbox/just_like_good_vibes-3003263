<?php

// https://github.com/tabvn/owl/blob/master/owl.module

namespace Drupal\owlcarousel;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class OwlCarouselOptions.
 */
class OwlCarouselOptions implements OwlCarouselOptionsInterface
{

    /**
     * Drupal\Core\Config\ConfigFactoryInterface definition.
     *
     * @var \Drupal\Core\Config\ConfigFactoryInterface
     */
    protected $configFactory;
    /**
     * Constructs a new OwlCarouselOptions object.
     */
    public function __construct(ConfigFactoryInterface $config_factory)
    {
        $this->configFactory = $config_factory;
    }

    protected function getSettings()
    {
        $settings = [
        "Navigation" => [
          "type" => "section"
        ],
        "Display" => [
        "type" => "section"
        ],
        "Advanced" => [
          "type" => "section"
        ],
        "nav" => [
          "default" => false,
          "type" => "boolean",
          "help" => "Show next/prev buttons.",
          "section" => "Navigation"
        ],
        "loop" => [
        "default" => false,
        "type" => "boolean",
        "help" => "Infinity loop. Duplicate last and first items to get loop illusion.",
        "section" => "Navigation"
        ],
        "mouseDrag" => [
        "default" => true,
        "type" => "boolean",
        "help" => "Mouse drag enabled.",
        "section" => "Navigation"
        ],
        "touchDrag" => [
        "default" => 1,
        "type" => "boolean",
        "help" => "Touch drag enabled.",
        "section" => "Navigation"
        ],
        "pullDrag" => [
        "default" => 1,
        "type" => "boolean",
        "help" => "Stage pull to edge.",
        "section" => "Navigation"
        ],
        "freeDrag" => [
        "default" => false,
        "type" => "boolean",
        "help" => "Item pull to edge.",
        "section" => "Navigation"
        ],
        "center" => [
        "default" => false,
        "type" => "boolean",
        "help" => "Center item. Works well with even an odd number of items.",
        "section" => "Display"
        ],
         "items" => [
        "default" => "3",
        "type" => "integer",
        "help" => "Show next/prev buttons.",
        "section" => "Display"
         ],
        "margin" => [
        "default" => "0",
        "type" => "integer",
        "help" => "margin-right(px) on item.",
        "section" => "Display"
        ],
        "stagePadding" => [
        "default" => 0,
        "type" => "integer",
        "help" => "Padding left and right on stage (can see neighbours).",
        "section" => "Display"
        ],
        "merge" => [
        "default" => false,
        "type" => "boolean",
        "help" => "Merge items. Looking for data-merge='{number}' inside item.",
        "section" => "Display"
        ],
        "mergeFit" => [
        "default" => false,
        "type" => "boolean",
        "help" => "Fit merged items if screen is smaller than items value.",
        "section" => "Display"
        ],
        "autoWidth" => [
        "default" => false,
        "type" => "boolean",
        "help" => "Set non grid content. Try using width style on divs.",
        "section" => "Display"
        ],
        "startPosition" => [
        "default" => 0,
        "type" => "string",
        "help" => "Start position or URL Hash string like '#id'.",
        "section" => "Navigation"
        ],
        "URLhashListener" => [
        "default" => false,
        "type" => "boolean",
        "help" => "Listen to url hash changes. data-hash on items is required.",
        "section" => "Navigation"
        ],
        "rewind" => [
        "default" => true,
        "type" => "boolean",
        "help" => "Go backwards when the boundary has reached.",
        "section" => "Navigation"
        ],
        "navText" => [
        "default" => "[&#x27;next&#x27;,&#x27;prev&#x27;]",
        "type" => "string",
        "help" => "array (HTML allowed.)",
        "section" => "Display"
        ],
        "navElement" => [
        "default" => "div",
        "type" => "string",
        "help" => "DOM element type for a single directional navigation link.",
        "section" => "Display"
        ],
        "slideBy" => [
        "default" => "1",
        "type" => "string",
        "help" => "Number/String. Navigation slide by x. 'page' string can be set to slide by page.",
        "section" => "Navigation"
        ],
        "slideTransition" => [
        "default" => "",
        "type" => "string",
        "help" => "You can define the transition for the stage you want to use eg. linear.",
        "section" => "Navigation"
        ],
        "dots" => [
        "default" => true,
        "type" => "boolean",
        "help" => "Show dots navigation.",
        "section" => "Display"
        ],
        "dotsEach" => [
        "default" => "",
        "type" => "string",
        "help" => "Number/Boolean. Show dots each x item.",
        "section" => "Navigation"
        ],
        "dotsData" => [
        "default" => false,
        "type" => "boolean",
        "help" => "Used by data-dot content.",
        "section" => "Display"
        ],
        "lazyLoad" => [
        "default" => false,
        "type" => "boolean",
        "help" => "Lazy load images. data-src and data-src-retina for highres. Also load images into background inline style if element is not <img>",
        "section" => "Display"
        ],
        "lazyLoadEager" => [
        "default" => 0,
        "type" => "number",
        "help" => "Eagerly pre-loads images to the right (and left when loop is enabled) based on how many items you want to preload.",
        "section" => "Display"
        ],
        "autoplay" => [
        "default" => false,
        "type" => "boolean",
        "help" => "Autoplay.",
        "section" => "Navigation"
        ],
        "autoplayTimeout" => [
        "default" => 5000,
        "type" => "integer",
        "help" => "Autoplay interval timeout.",
        "section" => "Navigation"
        ],
        "autoplayHoverPause" => [
        "default" => false,
        "type" => "boolean",
        "help" => "Pause on mouse hover.",
        "section" => "Navigation"
        ],
        "autoplaySpeed" => [
        "default" => false,
        "type" => "integer",
        "help" => "Number/Boolean. autoplay speed.",
        "section" => "Navigation"
        ],
        "navSpeed" => [
        "default" => false,
        "type" => "integer",
        "help" => "Number/Boolean. Navigation speed.",
        "section" => "Navigation"
        ],
        "dotsSpeed" => [
        "default" => false,
        "type" => "integer",
        "help" => "Number/Boolean. Pagination speed.",
        "section" => "Navigation"
        ],
        "dragEndSpeed" => [
        "default" => false,
        "type" => "integer",
        "help" => "Number/Boolean. Drag end speed.",
        "section" => "Navigation"
        ],
        "callbacks" => [
        "default" => true,
        "type" => "boolean",
        "help" => "Enable callback events.",
        "section" => "Advanced"
        ],
        "responsiveRefreshRate" => [
        "default" => 200,
        "type" => "integer",
        "help" => "Responsive refresh rate.",
        "section" => "Display"
        ],
        "video" => [
        "default" => false,
        "type" => "boolean",
        "help" => "Enable fetching YouTube/Vimeo/Vzaar videos.",
        "section" => "Display"
        ],
        "videoHeight" => [
        "default" => false,
        "type" => "integer",
        "help" => "Number/Boolean. Set height for videos.",
        "section" => "Display"
        ],
        "videoWidth" => [
        "default" => false,
        "type" => "integer",
        "help" => "Number/Boolean. Set width for videos.",
        "section" => "Display"
        ],
        "animateOut" => [
        "default" => false,
        "type" => "string",
        "help" => "String/Boolean. Class for CSS3 animation out.",
        "section" => "Advanced"
        ],
        "animateIn" => [
        "default" => false,
        "type" => "string",
        "help" => "String/Boolean. Class for CSS3 animation in.",
        "section" => "Advanced"
        ],
        "fallbackEasing" => [
        "default" => "swing",
        "type" => "string",
        "help" => "Easing for CSS2 $.animate.",
        "section" => "Display"
        ],
        "itemElement" => [
        "default" => "div",
        "type" => "string",
        "help" => "DOM element type for owl-item.",
        "section" => "Advanced"
        ],
        "stageElement" => [
        "default" => "div",
        "type" => "string",
        "help" => "DOM element type for owl-stage.",
        "section" => "Advanced"
        ],
        "navContainer" => [
        "default" => false,
        "type" => "string",
        "help" => "Set your own container for nav.",
        "section" => "Advanced"
        ],
        "dotsContainer" => [
        "default" => false,
        "type" => "string",
        "help" => "Set your own container for nav.",
        "section" => "Advanced"
        ],
        "checkVisible" => [
        "default" => true,
        "type" => "boolean",
        "help" => "If you know the carousel will always be visible you can set `checkVisibility` to `false` to prevent the expensive browser layout forced reflow the $element.is(':visible') does.",
        "section" => "Advanced"
        ],











        ];
        return $settings;
    }

    protected function getResponsivePresets()
    {
        $responsive_presets = ["0", "480", "768", "1024", "1280"];
        return $responsive_presets;
    }

    /*
    * return default settings
    *
    * @return array
    */
    public function defaultSettings()
    {
        $settings = $this->getSettings();
        $default_settings = array();
        foreach($settings as $key => $item) {
            // section are not settings, they are just here to organize settings
            if ($item["type"] == "section") { continue;
            }
            if (array_key_exists("default", $item)) {
                if (array_key_exists("section", $item)) {
                    $default_settings[$item["section"]][$key] = $item["default"];
                    $default_settings[$item["section"]]["enable-".$key] = false;
                } else {
                    $default_settings[$key] = $item["default"];
                    $default_settings["enable-".$key] = false;
                }
            }
        }
        $default_settings["responsive"] = [];
        $responsive_presets = $this->getResponsivePresets();
        foreach($responsive_presets as $responsive_preset => $responsive_preset_value) {
            $default_settings["responsive"][''.$responsive_preset] = [
            "enabled" => ["default" => true],
            "breakpoint" => ["default" => $responsive_preset_value],
            ];
            foreach($settings as $key => $item) {
                // section are not settings, they are just here to organize settings
                if ($item["type"] == "section") { continue;
                }
                if (array_key_exists("default", $item)) {
                    if (array_key_exists("section", $item)) {
                        $default_settings["responsive"][''.$responsive_preset][$item["section"]][$key] = $item["default"];
                        $default_settings["responsive"][''.$responsive_preset][$item["section"]]["enable-".$key] = false;
                    } else {
                        $default_settings["responsive"][''.$responsive_preset][$key] = $item["default"];
                        $default_settings["responsive"][''.$responsive_preset]["enable-".$key] = false;
                    }
                }
            }
        }
        return [
        "owlcarousel" => ["default" => $default_settings],
        "owlcarousel_raw" => ["default" => "blabla"],
        "enable-owlcarousel_raw" => ["default" => false]
        ];
    }

    /*
    * format settings
    *
    * @param $settings input settings
    *
    * @return array settings formatted
    */
    public function formatSettings($settings)
    {

        $owlcarousel_settings = [];
        if (array_key_exists("enable-owlcarousel_raw", $settings) && $settings["enable-owlcarousel_raw"]) {
            if (array_key_exists("owlcarousel_raw", $settings) && (strlen(trim($settings["owlcarousel_raw"])) > 0)) {
                $owlcarousel_settings = (array) json_decode($settings["owlcarousel_raw"]);
                return $owlcarousel_settings;
            }
        }
        $settings = $settings["owlcarousel"];
        $all_settings = $this->getSettings();
        foreach($all_settings as $key => $item) {
            $this->formatSettingsElement($settings, $key, $item);
        }
        // TODO
        $responsive_items = (array)$settings["responsive"];
        // reset final responsive items
        $settings["responsive"] = [];
        foreach($responsive_items as $reponsive_key => $responsive_values) {
            // check if responsive enabled
            if ((int)$responsive_values["enabled"] > 0) {
                $responsive_value = (int)$responsive_values["breakpoint"];
                unset($responsive_values["enabled"]);
                unset($responsive_values["breakpoint"]);
                $settings["responsive"]["".$responsive_value] = $responsive_values;
                foreach($all_settings as $key => $item) {
                    $this->formatSettingsElement($settings["responsive"]["".$responsive_value], $key, $item);
                }
                if (count($settings["responsive"]["".$responsive_value]) == 0) {
                    unset($settings["responsive"]["".$responsive_value]);
                }
                if ($reponsive_key != "".$responsive_value) {
                    unset($settings["responsive"][$reponsive_key]);
                }
            } else {
                unset($settings["responsive"][$reponsive_key]);
            }

        }
        return $settings;
    }

    protected function formatSettingsElement(&$settings, &$key, &$element)
    {
        if ($element["type"] == "section") {
            return;
        }
        $section = "";
        if (array_key_exists("section", $element)) {
            $section = $element["section"];
        }
        $enabled = false;
        if ($section) {
            $enabled = (bool) $settings[$section]["enable-".$key];
        } else {
            $enabled = (bool) $settings["enable-".$key];
        }

        switch($element["type"]) {
        case "boolean":
            if ($section) {
                $settings[$key] = (bool) $settings[$section][$key];
            } else {
                $settings[$key] = (bool) $settings[$key];
            }
            break;

        case 'integer':
            if ($section) {
                $settings[$key] = (int) $settings[$section][$key];
            } else {
                $settings[$key] = (int) $settings[$key];
            }
            break;

        case 'string':
            if ($section) {
                $settings[$key] = (string) $settings[$section][$key];
            } else {
                $settings[$key] = (string) $settings[$key];
            }
            break;
        }
        if ($section) {
            unset($settings[$section][$key]);
            unset($settings[$section]["enable-".$key]);
            if (count($settings[$section]) == 0) {
                unset($settings[$section]);
            }
        } else {
            unset($settings["enable-".$key]);
        }
        if (!$enabled) {
            unset($settings[$key]);
        }

    }


    protected function addFormElement(&$form, &$options, $key, $element, $to_merge = null, $add_enabler=false, $input_name_prefix="")
    {
        // https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Render%21Element%21FormElement.php/class/FormElement/8.6.x
        $form_element = [];
        if ($element["type"] == "section") {
            $add_enabler = false;
        }
        $default_value =
        array_key_exists("section", $element)? $options[$element["section"]][$key] : $options[$key];
        $default_value_enable =
        array_key_exists("section", $element)? $options[$element["section"]]["enable-".$key] : $options["enable-".$key];
        switch($element["type"]) {
        case "section":
            $form_element = array(
            '#type' => 'fieldset',
            '#title' => $key,
            '#description' => "",
            "#tree" => true,
            "#open" => false,
            );
            break;
        case "boolean":
            $form_element = array(
            '#type' => 'checkbox',
            '#title' => $key,
            '#return_value' => 1,
            '#default_value' => $default_value,
            '#description' => "",
            );
            break;
        case 'integer':
            $form_element = array(
            '#type' => 'number',
            '#title' => $key,
            '#default_value' => (int)$default_value,
            '#description' => "",
            );
            break;
        case 'string':
            $form_element = array(
            '#type' => 'textfield',
            '#title' => $key,
            '#default_value' => (string)$default_value,
            '#description' => "",
            );
            break;
        }
        if(array_key_exists("title", $element)) {
            $form_element["#title"] = t($element["title"]);
        } else {
            $form_element["#title"] = $key;
        }
        if(array_key_exists("help", $element)) {
            $form_element["#description"] = t($element["help"]);
        }
        $integer_params = [
        "min" => "min",
        "max" => "max",
        "weight" => "weight"];
        foreach($integer_params as $int_param => $target_form_param) {
            if (array_key_exists($int_param, $element)) {
                $form_element["#".$target_form_param] = (int) $element[$int_param];
            }
        }
        if (array_key_exists("section", $element)) {
            $section = $element["section"];
            if(!array_key_exists($section, $form)) {
                $form[$section] = [];
            }
            if ($add_enabler) {
                if (!array_key_exists("#title", $form_element)) {
                    \Drupal::logger('owl_carousel')->info("no title=><pre>".print_r($form_element, true)."</pre>");
                }
                $form[$section]["enable-".$key] = array(
                '#type' => 'checkbox',
                '#title' => t("Specify @title", ["@title" => $form_element["#title"]]),
                '#default_value' => $default_value_enable,
                '#description' => "",
                );
            }
            $form[$section][$key] = $form_element;
            if ($to_merge && is_array($to_merge)) {
                $form[$section][$key] = array_merge($form[$section][$key], $to_merge);
            }
            if ($add_enabler) {
                $form[$section][$key]["#states"]["disabled"][] = [
                ':input[name="'.$input_name_prefix.'['.$section.']['."enable-".$key.']"]' => [
                'checked' => false
                ]
                ];
                $form[$section][$key]["#states"]["visible"][] = [
                ':input[name="'.$input_name_prefix.'['.$section.']['."enable-".$key.']"]' => [
                'checked' => true
                ]
                ];
            }
            //\Drupal::logger("owlcarousel")->info("<pre>".print_r($form[$section][$key],TRUE)."</pre>");
        } else {
            if ($add_enabler) {
                $form["enable-".$key] = array(
                '#type' => 'checkbox',
                '#title' => t("Specify @title", ["@title" => $form_element["#title"]]),
                '#default_value' => $default_value_enable,
                '#description' => "",
                );
            }
            if (array_key_exists($key, $form)) {
                $form[$key] = array_merge($form[$key], $form_element);
            } else {
                $form[$key] = $form_element;
            }
            if ($to_merge && is_array($to_merge)) {
                $form[$key] = array_merge($form[$key], $to_merge);
            }
            if ($add_enabler) {
                $form[$key]["#states"]["disabled"][] = [
                ':input[name="'.$input_name_prefix.'['."enable-".$key.']"]' => [
                'checked' => false
                ]
                ];
            }

        }
    }
    /*
    *
    * @param $form form to fill
    */
    public function settingsForm(&$form, FormStateInterface $form_state, &$options)
    {
        \Drupal::logger('owl_carousel')->info("OPTions DEFAULT=><pre>".print_r($options, true)."</pre>");

        // https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html
        $form["enable-owlcarousel_raw"] = [
        '#type' => 'checkbox',
        '#title' => t("input OwlCarousel Settings settings in JSON instead of filling a form"),
        "#return_value" => "1",
        '#default_value' => $options["enable-owlcarousel_raw"],
        '#description' => "",
        ];
        $form["owlcarousel_raw"] = [
        "#type" => "textarea",
        "#title" => t("OwlCarousel Settings in JSON"),
        "#default_value" => $options["owlcarousel_raw"],
        "#description" => t("Fill in the OwlCarousel options here or fill in the form below"),
        "#rows" => 10,
        "#states" => [
        'visible' => [
          ':input[name="style_options[enable-owlcarousel_raw]"]' => [
            ['checked' => true]
          ],
        ]
        ]
        ];

        $form["owlcarousel"] = [
        "#type" => "fieldset",
        "#tree" => true,
        "#title" => t("OwlCarousel Settings"),
        "#open" => true,
        "#states" => [
        'visible' => [
          ':input[name="style_options[enable-owlcarousel_raw]"]' => [
            ['checked' => false]
          ],
        ]
        ]
        ];
        $form["owlcarousel"]["help"] = [
        "#type" => "markup",
        "#markup" => t(
            "See @link for more details",
            ["@link" => \Drupal\Core\Link::fromTextAndUrl(
                t("online documentation"),
                \Drupal\Core\Url::fromUri(
                    "https://owlcarousel2.github.io/OwlCarousel2/docs/api-options.html",
                    [
                    "external" => true,
                    "attributes" => [
                    "target" => "_blank"
                    ]
                    ]
                )
            )->toString()]
        ),
        ];
        $all_settings = $this->getSettings();
        foreach($all_settings as $key => $item) {
            $this->addFormElement(
                $form["owlcarousel"],
                $options["owlcarousel"],
                $key,
                $item,
                null,
                true,
                'style_options[owlcarousel]'
            );
        }
        $form["owlcarousel"]["responsive"] = [
        "#type" => "fieldset",
        "#tree" => true,
        "#title" => t("Responsive Settings")
        ];
        $responsive_presets = ["0", "480", "768", "1024", "1280"];
        foreach($responsive_presets as $responsive_preset => $responsive_preset_value) {
            $form["owlcarousel"]["responsive"][''.$responsive_preset] = [
            "#type" => "details",
            "#tree" => true,
            "#open" => true,
            '#collapsible' => false,
            "#title" => t("Responsive Breakpoint %number", ["%number" => ($responsive_preset + 1)]),
            ];
            $form["owlcarousel"]["responsive"][''.$responsive_preset]["enabled"] =  array(
            '#type' => 'checkbox',
            '#title' => t("Enable"),
            "#return_value" => "1",
            '#default_value' => $options["owlcarousel"]["responsive"]["".$responsive_preset]["enabled"],
            '#description' => "",
            );
            $form["owlcarousel"]["responsive"][''.$responsive_preset]["breakpoint"] =  array(
            '#type' => 'number',
            '#title' => t("Breakpoint"),
            '#min' =>  0,
            '#default_value' => $options["owlcarousel"]["responsive"]["".$responsive_preset]["breakpoint"],
            '#description' => "",
            "#states" => [
              'disabled' => [
              ':input[name="style_options[owlcarousel][responsive]['.$responsive_preset.'][enabled]"]' => [
              'checked' => false
              ],
              ]
            ]
            );
            /*$form["owlcarousel"]["responsive"][$responsive_preset]["data"] = [
            "#type" => "details",
            "#tree" => true,
            "#open" => false,
            "#states" => [
              'open' => [
              ':input[name="style_options[owlcarousel][responsive]['.$responsive_preset.'][enabled]"]' => [
              'checked' => TRUE
              ],
              ':input[name="style_options[owlcarousel][responsive]['.$responsive_preset.'][breakpoint]"]' => [
              '!value' => ''
              ],
              ],
            ],
            "#title" => t("Responsive Breakpoint %number configuration", ["%number" => ($responsive_preset + 1)]),
            ];
            */
            $to_merge = [
            "#states" => [
              /*"disabled" => [
              [
              ':input[name="style_options[owlcarousel][responsive]['.$responsive_preset.'][enabled]"]' => [
                'checked' => false
              ],
              ],
               "or",
              [
              ':input[name="style_options[owlcarousel][responsive]['.$responsive_preset.'][breakpoint]"]' => [
                'value' => ''
              ],
              ]

              ],
              */
              "visible" => [
              ':input[name="style_options[owlcarousel][responsive]['.$responsive_preset.'][enabled]"]' => [
                'checked' => true
              ],
              ':input[name="style_options[owlcarousel][responsive]['.$responsive_preset.'][breakpoint]"]' => [
                 '!value' => ''
              ],
              ]
            ]
            ];

            $all_settings = $this->getSettings();
            foreach($all_settings as $key => $item) {
                $this->addFormElement(
                    $form["owlcarousel"]["responsive"][$responsive_preset],
                    $options["owlcarousel"]["responsive"][$responsive_preset],
                    $key,
                    $item,
                    $to_merge,
                    true,
                    'style_options[owlcarousel][responsive]['.$responsive_preset.']'
                );
            }
        }

    }

    /*
    *
    * @param $form validate form to fill
    */
    public function validateForm(&$form, FormStateInterface $form_state)
    {
        $owlcarousel_raw = $form_state
        ->getValue(["style_options","owlcarousel_raw"]);
        if (strlen(trim($owlcarousel_raw)) > 0) {
            try {

                $decoded = json_decode($owlcarousel_raw);
                if (!$decoded || !is_object($decoded)) {
                    $form_state->setErrorByName("owlcarousel_raw", t("invalid format for ".$form["owlcarousel_raw"]["#title"]));
                } else {

                }
            } catch (\Exception $e) {
                $form_state->setErrorByName("owlcarousel_raw", t("invalid format for ".$form["owlcarousel_raw"]["#title"]));
            }
        }

    }






}
